# Instal·lació de VSCodium

> Instal·lació de la versió VSCode lliure de la telemetria de Microsoft.


Afegim la key del repositori:

```
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
```

Afegim el repositori:

```
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
```

Actualitzem la informació de tots els paquets (inclosos els del nou repositori)

```
sudo apt-get update
```

Instal·lem codium:

```
sudo apt install codium
```









## Links

+ [Article a Genbeta de VSCodium](https://www.genbeta.com/desarrollo/vscodium-version-vs-code-libre-telemetria-microsoft)
+ [Visual Studio Code a Debian](https://wiki.debian.org/VisualStudioCode)
+ [Instal·lació VSCodium](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo)

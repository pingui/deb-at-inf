# Instal·lació d'Adobe Digital Editions, Calibre i el plugin DeDRM a Debian 11

Aquesta guia es basa en el document [Install Adobe Digital Editions and Calibre DeDRM on Fedora](https://gist.github.com/rendyanthony/a1d6b830c33a5e0fcd8b4c86f3f0dbff) que al seu torn fa el mateix amb uns documents per a archlinux.

## Objectiu

El present *HowTo* preten ajudar a eliminar el DRM dels vostres llibres
electrònics i fer un us similar al que es faria amb un
de físic. En cap cas s'hauria de fer servir aquesta guia per altres fins,
especialment aquells que puguin perjudicar a l'autor de l'obra. 

## Instal·lació de wine

Com que ADE és una aplicació Windows, per excutar-la farem servir [Wine](https://www.winehq.org/).  

Configurem les variables d'entorn:

```
wine_install="$HOME/.adewine/"
export WINEPREFIX=$wine_install
export WINEARCH=win32
```


Instal·lem dependències:

```
sudo apt-get update
sudo apt-get install wine winetricks gnutls-bin:i386
```

Configurem _wine_:

```
wineboot
winetricks -q corefonts
winetricks -q dotnet35sp1  # aquí és necessita esperit zen
winetricks -q python27
```

## Instal·lació d'ADE:

Baixem el paquet d'Adobe Digitals Edition:

```
curl -L http://download.adobe.com/pub/adobe/digitaleditions/ADE_2.0_Installer.exe \
    -o /tmp/adeinstaller.exe
```

L'instal·lem:

```
wine /tmp/adeinstaller.exe
```

## Autorització de l'ordinador

> Nota: Es necessita un Adobe ID. Podeu trobar informació per crear-ne en aquest [enllaç](https://helpx.adobe.com/manage-account/using/create-update-adobe-id.html).


1. Executa ADE
2. Help > Authorize Computer
3. Selecciona eBook Vendor: Adobe ID
4. Introdueix la teva Addobe ID i el Password


## Extracció de la _Adobe Key_ necessària per _DeDRM_


Creem el directori *dedrm_plugins* a wine:

```
mkdir -p "$wine_install/drive_c/dedrm_plugins/"
```


Ens baixem el programa PyCrypto i l'instal·lem:

```
curl -l https://web.archive.org/web/20190603051225/hxxp://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe \
	-o /tmp/pycrypto.exe    
wine /tmp/pycrypto.exe
```

Ens baixem el programa de python que extraurà la clau que desbloquejarà el DRM:

```
curl -L https://raw.githubusercontent.com/apprenticeharper/DeDRM_tools/Python2/DeDRM_plugin/adobekey.py \
    -o "$wine_install/drive_c/dedrm_plugins/adobekey.py"
```

L'executem:

```
wine python "$wine_install/drive_c/dedrm_plugins/adobekey.py"
```

I si la resposta és:

```
Found 1 keys
```

Ens generarà el fitxer que conté la clau, _adobekey_1.der_:

```
ls $wine_install/drive_c/dedrm_plugins/
adobekey_1.der  adobekey.py
```

## Configuració de calibre i del plugin DeDRM

> Nota: Com que DeDRM funciona amb Python2 necessitem que el programa Calibre faci servir aquesta versió de Python. Les actuals versions de Calibre ja fa servir Python3. Una possibilitat és instal·lar mitjançant Flatpak.

Si no tenim instal·lat `flatpak`:

```
sudo apt-get install flatpak
```

Afegim repositori per a flatpak:

```
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

Instal·lem calibre. Això ens instal·larà la darrera versió:

```
sudo flatpak install flathub com.calibre_ebook.calibre
```

Però necessitem instal·lar una versió prèvia amb Python2, hem de veure el _commit_ corresponent per retrocedir a una versió de calibre amb python2 com per exemple la v5.10.1 (Veure aquest [enllaç](https://github.com/flatpak/flatpak/issues/3097) ):
 
```
sudo flatpak update --commit=c0bb62d94103089757792f4d5ded405b182e772cca1ff2eaac6203e1b4d42cca com.calibre_ebook.calibre
```

Baixem el plugin DeDRM i descomprimim:

```
cd /tmp
wget https://github.com/apprenticeharper/DeDRM_tools/releases/download/v7.2.1/DeDRM_tools_7.2.1.zip
```
 
Obrim Calibre i instal·lem el plugin:

+ Preferences > Plugins > Load plugin from file. Seleccionem el plugin DeDRM, en format zip. (No el zip original tools)
+ Busquem el plugin i el seleccionem per configurar.
	- Seleccionem Adobe Digital Editions ebooks
	- Seleccionem _Import Exiting Keyfiles_
		- Naveguem fins a `$HOME/.adewine/drive_c/dedrm_plugins`
		- Seleccionem `adobekey_1.der`

## Eliminació del DRM

+ Obrim ADE
+ Arrosseguem el fitxer de tipus `.acsm` (o sigui el fitxer que ens donen quan comprem el llibre) a la finestra d'ADE
	- Això ens baixarà el fitxer encriptat amb DRM al directori `$HOME/Documents/My Digital Editions` 
+ Obrim Calibre
+ Afegim el fitxer encriptat a Calibre
	- Trobarem el fitxer desencriptat `.epub` al directori `/el path que li hagis posat al teu Calibre Library`

Et voilà ... un jour après! :D

## LINKS

+ [Installing ADE on Arch Linux via WINE](https://github.com/apprenticeharper/DeDRM_tools/wiki/Installing-ADE-on-Arch-Linux-via-WINE)
+ [Exactly how to remove DRM](https://github.com/apprenticeharper/DeDRM_tools/wiki/Exactly-how-to-remove-DRM)

## TROUBLESHOOTING

+ Si instal·lem paquet _gnutls-bin_ de 64 bits en comptes de 32 bits no ens
  funcionarà i haurem d'eliminar-ho tot de nou i tornar a instal·lar.

+ Si tot ha anat bé i no elimina el DRM, mireu que el llibre no estigués afegit a calibre abans de tot el procès. Haureu d'eliminar-ho de calibre i tornar-ho a afegir.

+ Alerta DeDRM necessita python2 no python3.

+ Si instal·lem Calibre amb flatpak se suposa que no existien prèviament versions amb altres gestors de paquets. En cas contrari hem d'eliminar aquestes versions.

+ Per executar un paquet que s'acaba d'instal·lar amb flatpak es pot fer des de la línia d'ordres amb `flatpack run ...` o sortint de la sessió perquè aparegui com a eina gràfica a Gnome al següent login.


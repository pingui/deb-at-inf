#!/bin/bash
# Filename:		configure_finch_1.sh
# Author:		jamoros
# Date:			19/04/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./configure_finch_1.sh [arg1...]
# Description:	Script que crea udev rules per poder executar programes amb el robot
# 				sense necessitat de ser root ni fer sudo.
# 				No comprova que hi hagi les llibreries libudev.
# 				Comprova que l'usuari que executa l'script és root.	
# 				Comprova que existeixi el grup plugdev.
#

# Grup plug USB, fitxer i directori de regles udev
grup=plugdev
udev_rule_file=55-finch.rules
udev_rule_dir=/lib/udev/rules.d/
RULE1='SUBSYSTEM=="usb", ATTRS{idVendor}=="2354", ATTRS{idProduct}=="1111", MODE="0660", GROUP="plugdev"'
RULE2='KERNEL=="hidraw*", ATTRS{idVendor}=="2354", ATTRS{idProduct}=="1111", MODE="0660", GROUP="plugdev"'

# Si no som root sortim de l'script
if [[  $EUID -ne 0 ]]
then
	echo "Aquest script l'has d'executar com a root" 1>&2
	exit 1
fi

# Si no existeix el grup el creem
if ! getent group $grup &> /dev/null
then
	echo "Creant el grup $grup ..."
	/sbin/groupadd $grup
fi

# Creem un fitxer amb regles udev per donar permisos als membres del grup
echo "$RULE1" > ${udev_rule_dir}${udev_rule_file}
echo "$RULE2" >> ${udev_rule_dir}${udev_rule_file}

# Fem que es tornin a llegir les regles udev
udevadm control --reload

# Mostrem quina ordre permet afegir un usuari al grup plugdev

echo -e "\nSi vols afegir l'usuari USER al grup $grup executa com a root l'ordre:\n\ngpasswd -a USER $grup\n\non USER és l'usuari que vols afegir\nRecorda desconnectar el cable USB i tornar-lo a connectar\n"


# vim:ai:ts=4:sw=4:syntax=sh

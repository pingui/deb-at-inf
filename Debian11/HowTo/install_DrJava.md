## Instal·lació de la llançadera DrJava


Descarreguem el comprimit [drjava-debian11-gnome3.tar.xz](drjava-debian11-gnome3.tar.xz) i el movem a `/tmp`

Com a root el descomprimim:

```
tar xJvf drjava-debian11-gnome3.tar.xz
```

Un cop descomprimit ens col·loquem al directori que s'ha creat:

```
cd drjava-debian11-gnome3
```

Executem l'script:

```
./install-drjava.sh
```

Ara obrim una terminal (o terminator) i executem amb el nostre usuari (no root) la següent instraucció:

```
echo -e "#DrJava configuration file\njavac.location = /usr/lib/jvm/adoptopenjdk-8-openj9-amd64/lib/tools.jar" >> ~/.drjava
```

et voila!


Si algun dia volem desinstal·lar-ho només haurem de fer servir l'altre script `uninstall-drjava.sh`.

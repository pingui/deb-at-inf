## Instal·lació de Debian Bullseye (11) a un portàtil Lenovo ThinkBook 14 IIL Intel Core i3-1005G1/8GB/256GB SSD/14"


### Imatge de la instal·lació

Podem escollir diferents imatges de Debian en funció del tipus d'instal·lació, arquitectura, etc ...

També es pot donar el cas de que el nostre portàtil tingui algun element de
maquinari que necessiti firmware propietari. Per si de cas és el nostre cas,
escollirem aquesta opció:
 
Baixem la imatge estable de Debian:

```
wget https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/amd64/iso-cd/firmware-11.0.0-amd64-netinst.iso
```

### Creació del pen USB arrencable

Utilitzarem un pen USB (com s'ha escollit una imatge de menys d'1 GB qualsevol
memòria flash d'aquesta mida serà suficient.). Evidentment, tot el que hi ha hagi al pen USB serà eliminat.

Ens col·loquem al directori a on es troba la imatge *iso* que ens hem baixat i
esbrinem quina és la lletra del nostre dispositiu pen USB. Un cop l'hem
"punxat" mirem si es tracta de la `/dev/sdb` o `/dev/sdc` ....

Suposem que és /dev/sdb copiarem la imatge i sincronitzarem amb les següents ordres com a usuari root:

```
cat firmware*.iso > /dev/sdb 	# pot trigar una estona
sync				# pot trigar una estona
```

### Configuració de la BIOS:

Entrem a la BIOS, amb F1 i si hi demana un password, com per exemple podria ser
*3duc4c102020* el posem.

Qué podem fer a la BIOS?

+ Eliminar el password de la BIOS ( *Security* -> *Set Administrator Password* ->
  *Enter* -> *Current Password* -> Finalment repetim passwords buits.
+ Deshabilitar el *Secure Boot* a *Security*.
+ Habilitar *USB Boot* a *Boot*.

Sortirem de la BIOS desants els canvis. Recordem que F12 ens mostra un menú
d'arrencada per escollir que arrenqui del nostre pen USB a on es troba la
imatge de Debian. 

### Instal·lació de Debian

Farem una *Graphical Install*

+ Seleccionem llengua d'instal·lació *English*.
+ Seleccionem com a *locale* *Spain* (_other_ -> _Europe_ -> _Spain_ )
+ Seleccionem com a teclat *Catalan*.


+ Si tot i així no detecta la targeta wireless podem fer servir una connexió ethernet.
+ Posteriorment ens demanarà pel host, el domini, el password de root, el d'usuari.
+ Demana la hora: Madrid

+ Tipus d'instal·lació: _Manual_
+ Necessitem suficient espai per a les particions necessàries:

	+ Si la instal·lació és BIOS LEGACY:

		- Partició pel sistema
			- Mida: si és possible 50 GB
			- Partició lògica
			- _Use as_: _Ext4 ..._
			- _Format the partition_: _yes, format it_
			- _Mount point_: _/ - the root file system_
			- _Label_: DEBIAN11 (per exemple)
			- Ja ho tenim: _Done setting up the partition_

		- Partició swap:
			- Mida: 5 GB
			- Partició lògica
			- _Use as_: _swap area_

	+ Si la instal·lació és BIOS UEFI haurem d'afegir una partició:

		-Partició amb informació de l'arrencada

			- Mida 500 MB
			- _Use as_: _EFI System partition_ o _vfat_

	Arribats aquí, és ara quan acceptem i es realitzen els canvis anteriors.

+ Mirror (rèplica), escollim:
	- _Spain_
	- _ftp.caliu.cat_
	- Cap informació extra de HTTP Proxy (o sigui ho deixem en blanc)

+ No participem en la _contest survey_

+ Software selection

	+ Podem deixar les opcions que hi ha per defecte o escollir algun altre escriptori o afegir algun server si els necessitem.


+ Falta acabar


* Si no s'instal·la sol després d'instal·lar un kernel més nou instal·lem alguns firmwares extres com per exemple:

firmware-iwlwifi (vigilant que la versió no sigui antiga, del kernel antic)
firmware-misc-nonfree
firmware-realtek

+ tweaks -> perquè funcioni el botó dret
+ natural scrolling ?




**Continuarem amb la instal·lació. ÉS necessita configurar els backports perquè
funcioni la gràfica.**

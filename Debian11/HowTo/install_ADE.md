# Instal·lació de Adobe Digital Edition a Debian 11


> Suposarem que tenim una Debian 11 (Bullseye) a una màquina de 64 bits

## 1 Instal·lació de winehq

Afegim l'arquitectura i386 al repositori:

```
sudo dpkg --add-architecture i386
```

Importem la clau de winehq (amb apt-key que és *obsolet*, hauriem de fer servir OpenPGP key)

```
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
```

Afegim el repositori de la distro _Bullseye_ 

```
sudo echo "deb https://dl.winehq.org/wine-builds/debian/ bullseye main" >  /tmp/winehq.list
sudo cp -v /tmp/winehq.list /etc/apt/sources.list.d/
```

Instal·lem la darrera versió de wine:

```
sudo apt update
sudo apt install --install-recommends winehq-stable
```

## 2 Configuració i instal·lacions extres de wine


A partir d'ara hi haurà molts warnings i errors que ens poden despistar. Si les
finestres del windows (recursivitat?) no es tanquen, continuem.


Instal·lem un nou prefix WINE i posem que la versió de Windows sigui XP

```
WINEPREFIX="$HOME/.wine32" WINEARCH=win32 winecfg
```

Desprès d'instal·lar diferents components s'obre un menu. A la pestanya
Applications hi ha un menú desplegable que ens permet escollir la versió del
windows: seleccionem XP

![imatge del menu de configuració de wine](imatges/wine_configuration.png)

Instal·lem `winetricks` i `cabextract`:

```
sudo apt-get install winetricks cabextract
```

Instal·lem el _framework .NET_ i les _TrueType core fonts_ amb `winetricks`:

```
WINEPREFIX="$HOME/.wine32" WINEARCH=win32 winetricks corefonts dotnet35sp1
```

## 3 Instal·lació d'ADE


Ens baixem la versió 3.0:

```
wget https://archive.org/download/ade-3.0-installer/ADE_3.0_Installer.exe
# wget http://download.adobe.com/pub/adobe/digitaleditions/ADE_3.0_Installer.exe
```

I finalment l'instal·lem:

```
WINEPREFIX="$HOME/.wine32" WINEARCH=win32 wine ADE_3.0_Installer.exe
```


## LINKS

+ [Instal·lació de winehq per a Debian Bullseye](https://wiki.debian.org/Wine#Debian_Bullseye)
+ [Instal·lació d'ADE amb wine](https://support.ebooks.com/hc/en-gb/articles/360000017116-Reading-an-ebook-on-a-Linux-computer)


# Possibles problemes


+ No deshabilitem per defecte firewalld.
+ No deshabilitem per defecte Selinux. Debian fa servir [AppArmor](https://www.sololinux.es/apparmor-vs-selinux/)
+ He trobat algunes errades amb les credentials de Kerberos 

+ A l''aula F2G he trobat alguna errada amb algun firmware, [es pot solucionar habilitant els repositoris non-free i instal·lant un paquet](https://unix.stackexchange.com/questions/384403/debian-stretch-failed-to-load-firmware-rtl-nic-rtl8168g-3-fw-2)
	
	Afegim firmware privatiu per a la targeta de xarxa i d'altres.

	apt-get install firmware-realtek firmware-misc-nonfree



+ scdaemon probably not installed






● sssd-nss.socket - SSSD NSS Service responder socket
     Loaded: loaded (/lib/systemd/system/sssd-nss.socket; enabled; vendor preset: enabled)
     Active: failed (Result: exit-code) since Thu 2021-09-09 16:00:01 CEST; 4min 55s ago
   Triggers: ● sssd-nss.service
       Docs: man:sssd.conf(5)
     Listen: /var/lib/sss/pipes/nss (Stream)
    Process: 496 ExecStartPre=/usr/libexec/sssd/sssd_check_socket_activated_responders -r nss (code=exited, status=17)
        CPU: 4ms

Warning: some journal files were not opened due to insufficient permissions.



   69  09/09/21 15:59:10 dmesg
   70  09/09/21 16:07:33 systemctl disable sssd-nss.socket 
   71  09/09/21 16:11:50 systemctl disable sssd-autofs.socket 
   72  09/09/21 16:12:03 systemctl disable sssd-pam-priv.socket 


```
systemctl disable ssh
```

en comptes de:

```
systemctl disable sshd
```

Fa que després, quan es vulgui habilitar el servei funcioni bé, de l\'altra manera no m'acaba de funcionar






****************

Alguns dels missatges amb warning i errors després de fer:

```
apt-get install krb5-user krb5-multidev libpam-mount sssd nfs-common autofs ufw curl -y 
```

```
Suggested packages:
  doc-base krb5-doc krb5-k5tls glibc-doc cifs-utils davfs2 sshfs xfsprogs hxtools open-iscsi watchdog adcli libsss-sudo sssd-tools libsasl2-modules-ldap
``


```
...

Creating config file /etc/default/autofs with new version
update-rc.d: warning: start and stop actions are no longer supported; falling back to defaults
Created symlink /etc/systemd/system/multi-user.target.wants/autofs.service → /lib/systemd/system/autofs.service.
Setting up libnss-sss:amd64 (2.4.1-2) ...
First installation detected...
Checking NSS setup...
Adding an entry for automount.
Setting up libkdb5-10:amd64 (1.18.3-6) ...
Setting up libini-config5:amd64 (0.6.1-2) ...
Setting up sssd-common (2.4.1-2) ...
Creating SSSD system user & group...
adduser: Warning: The home directory `/var/lib/sss' does not belong to the user you are currently creating.
Warning: found usr.sbin.sssd in /etc/apparmor.d/force-complain, forcing complain mode
Warning from /etc/apparmor.d/usr.sbin.sssd (/etc/apparmor.d/usr.sbin.sssd line 54): Warning failed to create cache: usr.sbin.sssd
Created symlink /etc/systemd/system/sssd.service.wants/sssd-autofs.socket → /lib/systemd/system/sssd-autofs.socket.
Created symlink /etc/systemd/system/sssd.service.wants/sssd-nss.socket → /lib/systemd/system/sssd-nss.socket.
Created symlink /etc/systemd/system/sssd.service.wants/sssd-pam-priv.socket → /lib/systemd/system/sssd-pam-priv.socket.
Created symlink /etc/systemd/system/sssd.service.wants/sssd-pam.socket → /lib/systemd/system/sssd-pam.socket.
Created symlink /etc/systemd/system/sssd.service.wants/sssd-ssh.socket → /lib/systemd/system/sssd-ssh.socket.
Created symlink /etc/systemd/system/sssd.service.wants/sssd-sudo.socket → /lib/systemd/system/sssd-sudo.socket.
Created symlink /etc/systemd/system/multi-user.target.wants/sssd.service → /lib/systemd/system/sssd.service.
sssd-autofs.service is a disabled or a static unit, not starting it.
sssd-nss.service is a disabled or a static unit, not starting it.
sssd-pam.service is a disabled or a static unit, not starting it.
sssd-ssh.service is a disabled or a static unit, not starting it.
sssd-sudo.service is a disabled or a static unit, not starting it.
A dependency job for sssd-autofs.socket failed. See 'journalctl -xe' for details.
A dependency job for sssd-nss.socket failed. See 'journalctl -xe' for details.
A dependency job for sssd-pam-priv.socket failed. See 'journalctl -xe' for details.
A dependency job for sssd-pam.socket failed. See 'journalctl -xe' for details.
A dependency job for sssd-ssh.socket failed. See 'journalctl -xe' for details.
A dependency job for sssd-sudo.socket failed. See 'journalctl -xe' for details.
Setting up sssd-proxy (2.4.1-2) ...
Setting up nfs-common (1:1.3.4-6) ...

Creating config file /etc/idmapd.conf with new version
Adding system user `statd' (UID 119) ...
Adding new user `statd' (UID 119) with group `nogroup' ...
Not creating home directory `/var/lib/nfs'.
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-client.target → /lib/systemd/system/nfs-client.target.
Created symlink /etc/systemd/system/remote-fs.target.wants/nfs-client.target → /lib/systemd/system/nfs-client.target.
nfs-utils.service is a disabled or a static unit, not starting it.
Setting up sssd-ad-common (2.4.1-2) ...
Created symlink /etc/systemd/system/sssd.service.wants/sssd-pac.socket → /lib/systemd/system/sssd-pac.socket.
sssd-pac.service is a disabled or a static unit, not starting it.
A dependency job for sssd-pac.socket failed. See 'journalctl -xe' for details.
Setting up sssd-krb5-common (2.4.1-2) ...
Setting up sssd-krb5 (2.4.1-2) ...
Setting up libkadm5srv-mit12:amd64 (1.18.3-6) ...
Setting up libc6-dev:amd64 (2.31-13) ...
Setting up sssd-ldap (2.4.1-2) ...
Setting up sssd-ad (2.4.1-2) ...
Setting up sssd-ipa (2.4.1-2) ...
Setting up krb5-user (1.18.3-6) ...
Setting up comerr-dev:amd64 (2.1-1.46.2-2) ...
Setting up sssd (2.4.1-2) ...
Setting up krb5-multidev:amd64 (1.18.3-6) ...
Processing triggers for man-db (2.9.4-2) ...
Processing triggers for sgml-base (1.30) ...
Processing triggers for libc-bin (2.31-13) ...
Processing triggers for rsyslog (8.2102.0-2) ...
Setting up libpam-mount-bin (2.18-1) ...
```


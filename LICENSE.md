Copyright (c) 2021 Manel Mellado, Julio Amorós (per a tot el repositori)

+ Tot el programari que forma part d'aquest repositori és lliure amb llicència GPL v3:
    *This is free software, licensed under the GNU General Public License v3. See [this link for more info](http://www.gnu.org/licenses/gpl.html) for more information.*

+ La resta de documentació està subjecta a una llicència de Reconeixement-CompartirIgual 4.0 Internacional de Creative Commons que podeu trobar [en aquest enllaç](https://creativecommons.org/licenses/by-sa/4.0/deed.ca).

# Revisió final del GRUB

**Aquestes operacions les han de realitzar els alumnes del matí, el 2on dia d'instal·lació.**

Ara configurarem el GRUB correctament per a l’arrancada dual.

1. Verificació de que el sistema s’ha iniciat amb la partició del matí (`/dev/sda5`).

    Executem:
    - Aules N2H, N2J:
        
		```
    	mount | grep '/dev/sda5 on /'
    	```
        
    - Aules N2I:
        
    	```
    	mount | grep '/dev/nvme0n1p5' 
    	```

    Si no hi ha sortida tornem a iniciar el sistema i ara triem correctament la partició al GRUB.

    Assegurar-se de que aquesta vegada s'ha escollit l'entrada correcta tornant a executar l'ordre anterior.
    (Aquest seria un primer exemple de bucle)

    (Més endavant, amb expressions regulars podrem fer una comprovació comuna com per exemple `mount | grep  '/dev/[^ ]\+5 .* / '`) 
 
2. Canviem alguns valors de configuració de GRUB:

    Podem editar el fitxer `/etc/default/grub`

    i canviem que el temps d'espera (timeout) que ens proporciona GRUB, de 5 segons a pausa (-1):

    ```
    GRUB_TIMEOUT=-1
    ```

    O alternativament podem fer:

    ```
    sed -i  -e s,'GRUB_TIMEOUT=5','GRUB_TIMEOUT=-1',  /etc/default/grub
    ```

    Recuperem per al grub del matí la informació de la instal·lació de la tarda:
    
    ```
    update-grub
    ```
    
    Ara tornarem a instal·lar el grub a la partició del matí:

    Aula N2H, N2J

	```
	grub-install /dev/sda
	```
    Aula N2I

	```
	grub-install /dev/nvme0n1
	```

    Fem una còpia de seguretat del fitxer `grub.cfg`:

    ```
    cp /boot/grub/grub.cfg /boot/grub/grub.backup
    ```

    Ara cal editar `/boot/grub/grub.cfg` i modificar les entrades de les
    particions **normals**, no les de _rescue_ o _advanced_. El que hem de
    verificar dins del fitxer `/boot/grub/grub.cfg` és el text de les
    etiquetes. Ha de quedar així (usant sempre caràcters ASCII, i mai accents,
    etc.):

    ```
    ...
    menuentry 'DEBIAN MATI' ...
    ...
    menuentry 'DEBIAN TARDA' ...
    ```

    Després de fer els canvis i sortir de l'editor podem "xequejar"
    si hem comès algun error:
    ```
    grub-script-check /boot/grub/grub.cfg
    ```
    Com sempre *no news, good news*

    Una manera alternativa de modificar les línies del `/boot/grub/grub.cfg` es fent servir l'eina gràfica grub-customizer:

    ```
    apt-get install grub-customizer
    ```

    I executant-la després.


<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->

# Deb at Inf

Intal·lacions del sistema operatiu Debian al departament d'Informàtica de l'Escola del Treball i configuracions i programari divers.
Migració de tot el departament de la distribució Fedora (RedHat) utilitzada des de l'any 2000.

L'antic repositori d'instal·lacions de Fedora (només hi ha documentació a gitlab des de l'any 2016) es pot trobar aqui: [fed-at-inf](https://gitlab.com/jordinas/fed-at-inf)


